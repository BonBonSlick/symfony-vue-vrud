window.Vue = require('vue/dist/vue.js');
window.Bus = new Vue();
window.axios = require('axios');
window.axios.defaults.headers.common = {
    'X-Requested-With': 'XMLHttpRequest'
};
if ($('#app').length > 0) {

    require('./load-components');

    const app = new Vue({
        el: '#app'
    });
}