<?php
declare(strict_types = 1);

namespace App\Tests;

use App\Domain\Invoice\InvoiceCategory;
use PHPUnit\Framework\TestCase;

class InvoiceCategoryUnitTest extends TestCase
{
    /**
     * @var InvoiceCategory
     */
    private $category;

    /**
     * Called before every test
     */
    protected function setUp()
    {
        $this->category = new InvoiceCategory('BonBonSlickCat');
    }

    /**
     * @test
     */
    public function InvoiceCategory_ValidName_ReturnNewInstance() : void
    {
        $this->assertInstanceOf(InvoiceCategory::class, new InvoiceCategory('BonBonSlickCat'));
    }

    /**
     * @test
     */
    public function InvoiceCategory_Name_ReturnStringName() : void
    {
        $this->assertInstanceOf(InvoiceCategory::class, $this->category);
        $this->assertSame('BonBonSlickCat', $this->category->name());
    }
}