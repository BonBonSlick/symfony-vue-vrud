<?php

namespace App\Tests;

use App\Domain\Invoice\Invoice;
use App\Domain\Invoice\InvoiceCategory;
use App\Domain\User\User;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class InvoiceUnitTest extends TestCase
{
    /**
     * @var Invoice
     */
    private $invoice;

    /**
     * @var User
     */
    private $user;

    /**
     * Called before every test
     * @throws \ReflectionException
     */
    protected function setUp()
    {
        /** @var User|MockObject $user */
        $user = $this->createMock(User::class);
        /** @var InvoiceCategory|MockObject $category */
        $category = $this->createMock(InvoiceCategory::class);
        $this->invoice = new Invoice(10.0, $category, $user);

        $this->user = new User('BonBonS');
    }

    /**
     * @test
     * @throws \ReflectionException
     */
    public function Invoice_ValidData_ReturnNewInstance() : void
    {
        /** @var User|MockObject $user */
        $user = $this->createMock(User::class);
        /** @var InvoiceCategory|MockObject $category */
        $category = $this->createMock(InvoiceCategory::class);
        $invoice = new Invoice(10.0, $category, $user);
        $this->assertInstanceOf(Invoice::class, $invoice);
    }

    /**
     * @test
     */
    public function Invoice_Vendor_Change() : void
    {
        $this->invoice->changeVendor($this->user);
        $this->assertSame('BonBonS', $this->invoice->vendor()->name());
    }

    /**
     * @test
     */
    public function Invoice_IsPaid_Change() : void
    {
        $this->invoice->changeIsPaid(true);
        $this->assertTrue( $this->invoice->isPaid());
    }

}
