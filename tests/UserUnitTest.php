<?php
declare(strict_types = 1);

namespace App\Tests;

use App\Domain\User\User;
use PHPUnit\Framework\TestCase;

class UserUnitTest extends TestCase
{
    /**
     * @var User
     */
    private $user;

    /**
     * Called before every test
     */
    protected function setUp()
    {
        $this->user = new User('BonBonSlick');
    }

    /**
     * @test
     */
    public function User_ValidName_ReturnNewInstance() : void
    {
        $this->assertInstanceOf(User::class, new User('BonBonS'));
    }

    /**
     * @test
     */
    public function User_Name_ReturnStringName() : void
    {
        $this->assertInstanceOf(User::class, $this->user);
        $this->assertSame('BonBonSlick', $this->user->name());
    }

    /**
     * @test
     */
    public function User_ValidString_ChangeName() : void
    {
        $new = 'newName';
        $this->user->changeName($new);
        $this->assertSame('newName', $this->user->name());
    }
}