![Demo Admin CRUD Symfony 4 + Vue 2](https://pp.userapi.com/c824603/v824603507/11d621/C1qMWAZpvpk.jpg)

##Project Set Up
- ``composer install``
- ``npm install``
- ``npm run dev``
- ``php bin/console cache:clear``
- ``php bin/console assets:install --symlink``

- **configure DB connection**
- **set up permissions**

- ``php bin/console doctrine:migrations:diff``
- ``php bin/console doctrine:migrations:migrate``
- ``php bin/console doctrine:fixtures:load``


##Additional

- ``./vendor/bin/simple-phpunit`` - run tests
- ``http://127.0.0.1_your_host_/api/doc`` - see api docs