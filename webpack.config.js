let Encore = require('@symfony/webpack-encore');

Encore
    .setOutputPath('public/build')
    .setPublicPath('/build')
    .enableSourceMaps(!Encore.isProduction())

    .addEntry('js/app', './assets/js/app.js')

    .enableVueLoader()
    .cleanupOutputBeforeBuild()
    .enableSassLoader()
;

module.exports = Encore.getWebpackConfig();