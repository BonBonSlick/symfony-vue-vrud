<?php
declare(strict_types = 1);

namespace App\Domain\Invoice;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Infrastructure\Persistence\Doctrine\DoctrineInvoiceCategoryRepository")
 * @ORM\Table(name="invoice_categories")
 */
class InvoiceCategory
{
    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @var string
     *
     * @ORM\Column(type="string", unique=true)
     *
     * @Groups({"embed"})
     */
    private $name;

    /**
     * InvoiceCategory constructor.
     *
     * @param string $name
     *
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function id() : int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function name() : string
    {
        return $this->name;
    }
}