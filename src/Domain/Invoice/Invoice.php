<?php
declare(strict_types = 1);

namespace App\Domain\Invoice;

use App\Domain\User\User;
use Carbon\Carbon;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Infrastructure\Persistence\Doctrine\DoctrineInvoiceRepository")
 * @ORM\Table(name="invoices")
 */
class Invoice
{
    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @Groups({"invoice_list", "invoice_edit"})
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     *
     * @Groups({"invoice_list", "invoice_edit"})
     */
    private $price;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     *
     * @Groups({"invoice_list", "invoice_edit"})
     */
    private $isPaid;

    /**
     * @var Carbon
     *
     * @ORM\Column(type="carbondatetime")
     *
     * @Groups({"invoice_list", "invoice_edit"})
     */
    private $createdAt;

    /**
     * @var Carbon|null
     *
     * @ORM\Column(type="carbondatetime", nullable=true)
     *
     * @Groups({"invoice_list", "invoice_edit"})
     */
    private $updatedAt;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Domain\User\User")
     * @ORM\JoinColumn(name="vendor", referencedColumnName="id")
     *
     * @Groups({"invoice_list", "invoice_edit"})
     */
    private $vendor;

    /**
     * @var InvoiceCategory
     *
     * @ORM\ManyToOne(targetEntity="App\Domain\Invoice\InvoiceCategory")
     * @ORM\JoinColumn(name="vendor", referencedColumnName="id")
     *
     * @Groups({"invoice_list", "invoice_edit"})
     */
    private $category;

    /**
     * Invoice constructor.
     *
     * @param float           $price
     * @param InvoiceCategory $category
     * @param User            $vendor
     */
    public function __construct(float $price, $category, $vendor)
    {
        $this->vendor = $vendor;
        $this->category = $category;
        $this->price = $price;
        $this->createdAt = new Carbon();
        $this->isPaid = false;
    }

    /**
     * @return int
     */
    public function id() : int
    {
        return $this->id;
    }

    /**
     * @return float
     */
    public function price() : float
    {
        return $this->price;
    }

    /**
     * @return bool
     */
    public function isPaid() : bool
    {
        return $this->isPaid;
    }

    /**
     * @param bool $isPaid
     */
    public function changeIsPaid(bool $isPaid) : void
    {
        $this->isPaid = $isPaid;
        $this->changeUpdatedAt(new Carbon());
    }

    /**
     * @return Carbon
     */
    public function createdAt() : Carbon
    {
        return $this->createdAt;
    }

    /**
     * @param Carbon $createdAt
     */
    public function changeCreatedAt(Carbon $createdAt) : void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return Carbon|null
     */
    public function updatedAt() : ?Carbon
    {
        return $this->updatedAt;
    }

    /**
     * @param Carbon $updatedAt
     */
    public function changeUpdatedAt(Carbon $updatedAt) : void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return User
     */
    public function vendor() : User
    {
        return $this->vendor;
    }

    /**
     * @param User $vendor
     */
    public function changeVendor(User $vendor) : void
    {
        $this->vendor = $vendor;
        $this->changeUpdatedAt(new Carbon());
    }

    /**
     * @return InvoiceCategory
     */
    public function category() : InvoiceCategory
    {
        return $this->category;
    }

    /**
     * @param InvoiceCategory $category
     */
    public function changeCategory(InvoiceCategory $category) : void
    {
        $this->category = $category;
        $this->changeUpdatedAt(new Carbon());
    }
}