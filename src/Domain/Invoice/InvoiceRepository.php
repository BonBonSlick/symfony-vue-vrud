<?php
declare(strict_types = 1);

namespace App\Domain\Invoice;

interface InvoiceRepository
{

    /**
     * Saves Entity to DB
     *
     * @param Invoice $entity
     */
    public function save(Invoice $entity);
}