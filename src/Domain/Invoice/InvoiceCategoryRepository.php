<?php
declare(strict_types = 1);

namespace App\Domain\Invoice;

interface InvoiceCategoryRepository
{
    /**
     * Saves Entity to DB
     *
     * @param InvoiceCategory $entity
     */
    public function save(InvoiceCategory $entity);
}