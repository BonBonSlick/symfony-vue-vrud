<?php
declare(strict_types = 1);

namespace App\Application\Controller;

use App\Domain\Invoice\Invoice;
use App\Domain\Invoice\InvoiceCategory;
use App\Domain\User\User;
use InvalidArgumentException;
use LogicException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Webmozart\Assert\Assert;

/**
 * @Route("invoices", name="admin.")
 */
class InvoiceController extends Controller
{
    /**
     * @Route(
     *     path="/",
     *     name="invoices",
     *     methods="GET",
     * )
     */
    public function list()
    {
        return $this->render('invoice/index.html.twig');
    }

    /**
     * @Route(
     *     path="/{id}/delete",
     *     name="invoice.delete",
     *     methods="POST",
     *     requirements={"id"="\d+"}
     * )
     * @ParamConverter("invoice", class="App\Domain\Invoice\Invoice")
     *
     * @param Invoice $invoice
     *
     * @throws LogicException
     *
     * @return JsonResponse
     */
    public function delete(Invoice $invoice) : JsonResponse
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($invoice);
        $em->flush();

        return new JsonResponse(['message' => 'Item was successfully deleted!', 'deleted' => true]);
    }

    /**
     * @Route(
     *     path="/{id}/pay",
     *     name="invoice.pay",
     *     methods="POST",
     *     requirements={"id"="\d+"}
     * )
     * @ParamConverter("invoice", class="App\Domain\Invoice\Invoice")
     *
     * @param Invoice $invoice
     *
     * @throws LogicException
     *
     * @return JsonResponse
     */
    public function paid(Invoice $invoice) : JsonResponse
    {
        $invoice->changeIsPaid(true);
        $em = $this->getDoctrine()->getManager();
        $em->persist($invoice);
        $em->flush();

        return new JsonResponse(['message' => 'Item was successfully updated!', 'updated' => true]);
    }

    /**
     * @Route(
     *     path="/save",
     *     name="invoice.save",
     *     methods="POST",
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws LogicException
     */
    public function save(Request $request) : JsonResponse
    {
        $vendorName = (string)$request->request->get('vendor');
        $categoryName = (string)$request->request->get('category');
        $price = (float)$request->request->get('price');
        try {
            Assert::float($price, 'The price must be an integer. Got: %s');
            Assert::greaterThan($price, 0, 'The price must be an integer. Got: %s');
            Assert::stringNotEmpty($vendorName, 'The vendor name must be an integer. Got: %s');
            Assert::stringNotEmpty($categoryName, 'The category name must be an integer. Got: %s');
            $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['name' => $vendorName]);
            if (!$user) {
                return new JsonResponse(
                    [
                        'message' => 'No such vendor.',
                        'created' => false,
                        'errors'  => true,
                    ]
                );
            }
            $category = $this->getDoctrine()->getRepository(InvoiceCategory::class)->findOneBy(['name' => $categoryName]);
            if (!$category) {
                return new JsonResponse(
                    [
                        'message' => 'No such category.',
                        'created' => false,
                        'errors'  => true,
                    ]
                );
            }
        }
        catch (InvalidArgumentException $exception) {
            return new JsonResponse(
                [
                    'message' => $exception->getMessage(),
                    'created' => false,
                    'errors'  => true,
                ]
            );
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist(
            new Invoice(
                $price,
                $category,
                $user
            )
        );
        $em->flush();

        return new JsonResponse(['message' => 'Item was successfully created!', 'created' => true]);
    }

}