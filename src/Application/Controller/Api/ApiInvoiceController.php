<?php
declare(strict_types = 1);

namespace App\Application\Controller\Api;

use App\Domain\Invoice\Invoice;
use Carbon\Carbon;
use LogicException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Swagger\Annotations as SWG;

/**
 * @Route("api", name="api.")
 */
class ApiInvoiceController extends Controller
{
    /**
     * @Route(
     *      path="/invoice-list",
     *      name="invoice",
     *      methods={"GET"},
     *      )
     *
     * @SWG\Tag(name="Invoice list")
     * @SWG\Response(
     *     response=200,
     *     description="Invoices list",
     * )
     *
     * @throws LogicException
     */
    public function index()
    {
        $entities = $this->getDoctrine()
        ->getRepository(Invoice::class)
        ->FindBy(array(), array('createdAt' => 'DESC'));
        return $this->json($entities, Response::HTTP_OK, [], ['groups' => ['invoice_list', 'embed']]);
    }
}
