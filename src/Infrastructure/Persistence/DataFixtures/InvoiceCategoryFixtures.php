<?php
declare(strict_types = 1);

namespace App\Infrastructure\Persistence\DataFixtures;

use App\Domain\Invoice\InvoiceCategory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class InvoiceCategoryFixtures extends Fixture
{
    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager) : void
    {
        $manager->persist(new InvoiceCategory('BonBonSlickCat'));
        $manager->flush();
    }
}