<?php
declare(strict_types = 1);

namespace App\Infrastructure\Persistence\DataFixtures;

use App\Domain\Invoice\Invoice;
use App\Domain\Invoice\InvoiceCategory;
use App\Domain\User\User;
use Carbon\Carbon;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Exception;
use Faker\Factory;
use InvalidArgumentException;

class InvoiceFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * {@inheritdoc}
     * @throws InvalidArgumentException
     * @throws Exception
     */
    public function load(ObjectManager $manager) : void
    {
        $categories = $manager->getRepository(InvoiceCategory::class)->findAll();
        $vendors = $manager->getRepository(User::class)->findAll();
        for ($i = 0; $i < 22; $i++) {
            $entity = new Invoice(random_int(0, 100000 ) / 100, $categories[0], $vendors[0]);
            if($i % 2 == 0){
                $entity->changeIsPaid(true);
            }
            $entity->changeCreatedAt(new Carbon());
            $manager->persist($entity);
        }
        $manager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function getDependencies() : array
    {
        return [
            UserFixtures::class,
            InvoiceCategoryFixtures::class,
        ];
    }
}