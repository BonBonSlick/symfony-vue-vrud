<?php
declare(strict_types = 1);

namespace App\Infrastructure\Persistence\DataFixtures;

use App\Domain\User\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager) : void
    {
        $manager->persist(new User('BonBonSlick'));
        $manager->flush();
    }
}