<?php
declare(strict_types = 1);

namespace App\Infrastructure\Persistence\Doctrine;

use App\Domain\Invoice\InvoiceCategory;
use App\Domain\Invoice\InvoiceCategoryRepository;
use Doctrine\ORM\EntityRepository;

class DoctrineInvoiceCategoryRepository extends EntityRepository implements InvoiceCategoryRepository
{
    /**
     * {@inheritdoc}
     */
    public function save(InvoiceCategory $entity)
    {
        $this->_em->persist($entity);
        $this->_em->flush();
        $this->_em->clear();
    }
}