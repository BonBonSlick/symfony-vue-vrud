<?php
declare(strict_types = 1);

namespace App\Infrastructure\Persistence\Doctrine;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use App\Domain\Invoice\Invoice;
use App\Domain\Invoice\InvoiceRepository;

class DoctrineInvoiceRepository extends EntityRepository implements InvoiceRepository
{

    /**
     * {@inheritdoc}
     */
    public function save(Invoice $entity)
    {
       $this->_em->persist($entity);
       $this->_em->flush();
       $this->_em->clear();
    }
}