<?php
declare(strict_types = 1);

namespace App\Infrastructure\Persistence\Doctrine;

use App\Domain\User\User;
use App\Domain\User\UserRepository;
use Doctrine\ORM\EntityRepository;

class DoctrineUserRepository extends EntityRepository implements  UserRepository
{
    /**
     * {@inheritdoc}
     */
    public function save(User $entity)
    {
        $this->_em->persist($entity);
        $this->_em->flush();
        $this->_em->clear();
    }
}